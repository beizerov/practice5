package ua.nure.beizerov.practice5;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Paths;


/**
 * This class is represent a Part5.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part5 {
	
	private static final int NUMBER_OF_THREADS;
	private static final int END_OF_RANGE_IN_DIGIT_ROW;
	private static final int LINE_SEPARATOR_LENGTH;
	
	private final Thread[] threads;
	
	private final RandomAccessFile randomAccessFile;

	
	static {
		NUMBER_OF_THREADS = 10;
		END_OF_RANGE_IN_DIGIT_ROW = 20;
		LINE_SEPARATOR_LENGTH = System.lineSeparator().length();
		
		try {
			Files.deleteIfExists(Paths.get("part5.txt"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public Part5() {
		try {
			this.randomAccessFile = new RandomAccessFile("part5.txt", "rw");
		} catch (FileNotFoundException e) {
			throw new IllegalStateException("File wasn't opened!");
		}
		
		this.threads = new DigitWriter[NUMBER_OF_THREADS];
		
		for (int i = 0; i < threads.length; i++) {
			threads[i] = new DigitWriter(i);
		}
	}


	public static void main(String[] args) {
		Part5 part5 = new Part5();
		
		part5.start();
		
		part5.printFile();
	}
	
	
	public void start() {
		for (Thread thread : threads) {
			thread.start();
		}
		
		for (Thread thread : threads) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void printFile() {
		try {
			randomAccessFile.seek(0);
			
			String line;
			while((line = randomAccessFile.readLine()) != null) {
				System.out.println(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	private final class DigitWriter extends Thread {
		
		private final int digit;
		private final int startPosition;
		private final RandomAccessFile randomAccessFile;
		
		private int position;
		
		
		private DigitWriter(int digit) {
			this.digit = digit;
			this.position = 0;
			
			if (digit == 0) {
				this.startPosition = 0;
			} else {
				this.startPosition = (digit == 0) ? 0  
						: (digit * (END_OF_RANGE_IN_DIGIT_ROW 
								+ LINE_SEPARATOR_LENGTH));
			}
			
			randomAccessFile = Part5.this.randomAccessFile;
		}

		
		private void write() {
			try {
				Thread.sleep(1);
				
				int pos = startPosition + position;
				
				if(pos == (startPosition 
						+ END_OF_RANGE_IN_DIGIT_ROW 
						+ LINE_SEPARATOR_LENGTH)
				) {
						randomAccessFile.seek(pos - 1);
						randomAccessFile.write(
								System.lineSeparator().getBytes()
						);
				} else {
						randomAccessFile.seek(pos);
						randomAccessFile.write('0' + digit);
				}
				
				position++;
			} catch (IOException | InterruptedException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void run() {
			try {
				Thread.sleep(digit * 10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			synchronized (DigitWriter.class) {
				int i = 0; 
				while (i <= END_OF_RANGE_IN_DIGIT_ROW + LINE_SEPARATOR_LENGTH) {
					i++;
					write();
				}
			}
		}		
	}
}
