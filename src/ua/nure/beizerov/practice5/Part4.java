package ua.nure.beizerov.practice5;


import java.time.Duration;
import java.time.Instant;

import ua.nure.beizerov.practice5.utility.FileReader;
import ua.nure.beizerov.practice5.utility.StringToMatrixConverter;


/**
 * This class is represent a Part5.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part4 {
	
	public static void main(String[] args) {
		int[][] matrix = new StringToMatrixConverter()
				.convert(new FileReader("Cp1251", "part4.txt")
				.readFile());
		
		Instant start = Instant.now();
		System.out.println(findMaxWithMultithreading(matrix));
		Instant finish = Instant.now();
		System.out.println(Duration.between(start, finish).toMillis());
		
		start = Instant.now();
		System.out.println(findMaxWithoutMultithreading(matrix));
		finish = Instant.now();
		System.out.println(Duration.between(start, finish).toMillis());
	}
	
	
	private static int max(int integer1, int integer2) {
		return (integer1 > integer2) ? integer1 : integer2;
	}
	
	private static int findMaxWithMultithreading(int[][] matrix) {
		int[] resultVector = new int[matrix.length];
		Thread[] threads = new Thread[matrix.length];

		for (int i = 0; i < threads.length; i++) {
			final int k = i;
			threads[i] = new Thread(() -> {
				for (int j = 0; j < matrix[0].length; j++) {

					try {
						Thread.sleep(1);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					
					resultVector[k] = max(resultVector[k], matrix[k][j]);
				}
			});
		}
		
		for (int i = 0; i < threads.length; i++) {
			threads[i].setName(String.valueOf(i));
		}
		
		for (Thread thread : threads) {
			thread.start();
		}
		
		for (Thread thread : threads) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		int maxNumber = 0;
		for (int i = 0; i < resultVector.length; i++) {
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			maxNumber = max(maxNumber, resultVector[i]);
		}
		
		return maxNumber; 
	}
	
	private static int findMaxWithoutMultithreading(int[][] matrix) {
		int maxNumber = matrix[0][0];
		
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				maxNumber = max(maxNumber, matrix[i][j]);
			}
		}
		
		return maxNumber;
	}
}
