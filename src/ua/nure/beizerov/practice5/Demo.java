package ua.nure.beizerov.practice5;

import ua.nure.beizerov.practice5.utility.MatrixWriter;
import ua.nure.beizerov.practice5.utility.RandomMatrixGenerator;

/**
 * This class is represent a demo.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Demo {

	public static void main(String[] args) {
		System.out.println("=========================== PART1");
		Part1.main(null);
		
		System.out.println("=========================== PART2");
		Part2.main(null);
		
		System.out.println("=========================== PART3");
		Part3.main(null);
		
		System.out.println("=========================== PART4");
		new MatrixWriter("part4.txt").write(new RandomMatrixGenerator(4, 100));
		Part4.main(null);
		
		System.out.println("=========================== PART5");
		Part5.main(null);
		
		System.out.println("=========================== PART6");
		Part6.main(null);
	}
}
