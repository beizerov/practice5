package ua.nure.beizerov.practice5;


/**
 * This class is represent a Part1.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part1 {

	public static void main(String[] args) {
		Thread[] threads = {
			new Thread() {

				@Override
				public void run() {
					work();
				}
			},
			new Thread(new Runnable() {

				@Override
				public void run() {
					work();
				}	
			}),
			new Thread(Part1::work)
		};

		for (Thread thread : threads) {
			thread.start();
			
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}	
	
	
	private static void work() {
		int i = 3;
		
		while (i > 0) {
			System.out.println(Thread.currentThread().getName());
			
			try {
				Thread.sleep(333);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			i--;
		}
	}
}
