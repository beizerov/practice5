package ua.nure.beizerov.practice5;


import java.io.IOException;


/**
 * This class is represent a Spam.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Spam {
	
	private final Thread[] threads;

	
	public Spam(String[] messages, int[] times) {
		this.threads = new Thread[messages.length];
		
		for (int i = 0; i < threads.length; i++) {
			threads[i] = new Worker(messages[i], times[i]);
		}
	}
	
		
	public static void main(String[] args) {
		Spam spam = new Spam(
				new String[] { "@@@", "bbbbbbb" }, new int[] { 333, 222 }
		);
		
		spam.start();
		
		try {
			int ch;
			while ((ch = System.in.read()) != '\n' && ch != '\r') {
				// The loop executed until it encounters a line separator.
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		spam.stop();
	}
	
	public void start() {
		for (Thread thread : threads) {
			thread.start();
		}
	}
	
	public void stop() {
		for (Thread thread : threads) {
			thread.interrupt();
		}
	}
	
	private static final class Worker extends Thread {
		private final String message;
		private final int times;
		
		
		private Worker(String message, int times) {
			this.message = message;
			this.times = times;
		}


		@Override
		public void run() {
			while (!isInterrupted()) {
				try {
					Thread.sleep(times);
					System.out.println(message);
				} catch (InterruptedException e) {
					break;
				}
			}
		}
	}
}
