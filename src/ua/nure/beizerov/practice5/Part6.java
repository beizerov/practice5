package ua.nure.beizerov.practice5;


/**
 * This class is represent a Part6.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part6 {

	private static final Object lock = new Object();
	
	public static void main(String[] args) {
		Thread thread = new Thread(()-> {
			Thread t = Thread.currentThread();
			
			synchronized (lock) {
				
				try {
					lock.wait();
				} catch (InterruptedException e) {
					return;
				}
				
				while(!t.isInterrupted()) {
					// The loop executes until current thread
					// is not interrupted.
				}
			}
		});
		
		thread.start();

		synchronized (lock) {
			long l = Integer.MAX_VALUE;
			while (l > 0) {
				l--;
			}
			System.out.println(thread.getState());

			
			try {
				lock.wait(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println(thread.getState());
		}

		thread.interrupt();
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(thread.getState());
	}
}
