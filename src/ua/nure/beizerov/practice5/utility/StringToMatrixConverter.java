package ua.nure.beizerov.practice5.utility;


/**
 * This class is represent a string to matrix converter.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class StringToMatrixConverter {

	public int[][] convert(String input) {
		String[] strVector = input.split(System.lineSeparator());
		String[][] strMatrix = new String[strVector.length][];
		
		for (int i = 0; i < strMatrix.length; i++) {
			strMatrix[i] = strVector[i].split("\\s+");
		}
		
		int[][] matrix = new int[strMatrix.length][strMatrix[0].length];
		
		for (int i = 0; i < strMatrix.length; i++) {
			for (int j = 0; j < strMatrix[0].length; j++) {
				matrix[i][j] = Integer.parseInt(strMatrix[i][j]);
			}
		}
		
		return matrix;
	}
}
