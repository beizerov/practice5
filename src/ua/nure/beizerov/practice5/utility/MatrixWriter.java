package ua.nure.beizerov.practice5.utility;


import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;


/**
 * This class represents a class that writes a matrix to a file.
 * 
 * @author Alexei Beizerov
 * @version 1.0
 */
public class MatrixWriter {
	
	private final String path;

	
	/**
	 * @param path	to the file to which data will be written.
	 */
	public MatrixWriter(String path) {
		this.path = path;
	}


	/**
	 * @param rmg random matrix generator
	 */
	public void write(RandomMatrixGenerator rmg) {
		try(PrintStream printStream = 
				new PrintStream(
				new BufferedOutputStream(
				new FileOutputStream(path)
		))) {
			printStream.append(
				rmg.getRandomIntegersMatrix()
			);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}