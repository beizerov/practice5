package ua.nure.beizerov.practice5.utility;


import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;


/**
 * This class is represent a file reader.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class FileReader {
	
	private final String encoding;
	private final String path;
	
	
    /**
	 * @param encoding
	 * @param path
	 */
	public FileReader(String encoding, String path) {
		this.encoding = encoding;
		this.path = path;
	}

	
	/**
	 * @return a string representation of the file.
	 */
	public String readFile() {
        StringBuilder fileResource = new StringBuilder();
        
        try (BufferedReader reader = Files.newBufferedReader(
        		Paths.get(path), Charset.forName(encoding)
        	)) {
        	
        	String line = null;
            while ((line = reader.readLine()) != null) {
                fileResource
                	.append(line)
                	.append(System.lineSeparator());
            }
        } catch (IOException e) {
			e.printStackTrace();
		}
        
        return fileResource.toString();
    }
}
