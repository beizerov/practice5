package ua.nure.beizerov.practice5.utility;


import java.security.SecureRandom;


/**
 * This class represents a class that generates a matrix of random integers.
 * 
 * @author Alexei Beizerov
 * @version 1.0
 */
public class RandomMatrixGenerator {
	
	private static final int SEED_FOR_RANDOM = 1000;
	
	private final int m;
	private final int n;
	
	
	/**
	 * @param m		number of rows.
	 * @param n		number of columns.
	 */
	public RandomMatrixGenerator(int m, int n) {
		this.m = m;
		this.n = n;
	}


	/**
	 * @return a string representation of the matrix row.
	 */
	private String getRandomNumbersRow() {
		StringBuilder output = new StringBuilder();	
	
		for (int i = 0; i < n; i++) {
			output.append(String.format(
					"%3d ", new SecureRandom().nextInt(SEED_FOR_RANDOM))
			);
		}
		
		return output.toString().trim();
	}
	
	/**
	 * @return a string representation of the matrix.
	 */
	public String getRandomIntegersMatrix() {
		StringBuilder output = new StringBuilder();	
		
		for (int i = 0; i < m; i++) {
			output.append(String.format("%s%n", getRandomNumbersRow()));
		}
		
		return output.toString().trim();
	}
}
