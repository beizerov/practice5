package ua.nure.beizerov.practice5;

import java.io.IOException;
import java.io.InputStream;

/**
 * This class is represent a Part2.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part2 {
	
	private static final InputStream STD_IN = System.in;

	public static void main(String[] args) {
		System.setIn(new InputStream() {
			private final byte [] buffer = System.lineSeparator().getBytes();
			private int index;
			
			@Override
			public int read() throws IOException {
				if(index == 0) {
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				
				if (index < buffer.length) {
					return buffer[index++];
				}
				
				return -1;
			}
		});
		
		Thread thread = new Thread(() -> Spam.main(null));
		
		thread.start();
		
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.setIn(STD_IN);
	}
}
