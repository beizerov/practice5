package ua.nure.beizerov.practice5;


/**
 * This class is represent a Part3.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part3 {
	
	private int counter;
	private int counter2;

	private final Thread[] threads;
	private final int n; 
	private final int k; 
	private final int t;
	

	/**
	 * @param n		number of threads.
	 * @param k		times repeated in a loop.
	 * @param t		sleep time in milliseconds.
	 */
	public Part3(int n, int k, int t) {
		this.n = n;
		this.k = k;
		this.t = t;
		
		threads = new Thread[n];
	}


	public static void main(String[] args) {
		Part3 p = new Part3(3, 5, 100);
		p.test();
		p.reset();
		p.testSync();
	}

	
	public void reset() {
		counter = counter2 = 0;
	}
	
	private void work() {
		for (int j = 0; j < k; j++) {
			
			System.out.printf("%s %s%n", counter, counter2);
			
			counter++;
			
			try {
				Thread.sleep(t);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			counter2++;
		}
	}
	
	private void startThreads() {
		for (Thread thread : threads) {
			thread.start();
		}
	}
	
	private void joinThreads() {
		for (Thread thread : threads) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void test() {
		for (int i = 0; i < n; i++) {
			threads[i] = new Thread(this::work);
		}
		
		startThreads();
		joinThreads();
	}
	
	public void testSync() {
		for (int i = 0; i < n; i++) {
			threads[i] = new Thread(() ->  {
				synchronized (Part3.class) {
					work();
				}
			});
		}
		
		startThreads();
		joinThreads();
	}
}
